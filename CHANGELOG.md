# Change Log
All notable changes to the "crestron-simpl--code-snippets" extension will be documented in this file.

## `1.1.1`
- A few minor tweaks.

## `1.1.0`
- Initial release
- Removed snippet portion from main plugin and put it into standalone.