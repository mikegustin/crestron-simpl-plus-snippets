# crestron-simpl--code-snippets README

## Features

Optional code Snippets for Crestron Simpl+

## Requirements

This extension requires mwgustin.crestron-simpl-plus. 

## Release Notes

### 1.1.1
Minor tweaks.

### 1.1.0
Initial release/separated from primary plugin.

**Enjoy!*